package dev.stevl.bguploader;

import dev.stevl.bguploader.logic.ScriptExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SteVL
 */
public class Main {
    
    public static void main(String[] args) {
        try {
            new ScriptExecutor().execute();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
