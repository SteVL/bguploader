package dev.stevl.bguploader.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Vladislav Stebenev
 */
public class FileSettingsHandler implements SettingsHandler {

    private String fileName;
    private String filePath;
    private FTPAuthData authData;

    private String remoteCatalog;

    public FileSettingsHandler() throws Exception {
        readSettingsFromTxtFile();
    }

    @Override
    public String getFilePath() {
        return filePath;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public FTPAuthData getAuthData() {
        return authData;
    }

    @Override
    public String getRemoteCatalog() {
        return remoteCatalog;
    }

    /**
     * Читает настройки из текстового файла
     * <p>
     * Settings structure:<br>
     * Absolute file path, Filename with extension, FTP catalog for uploading,
     * server, port, user, password
     * </p>
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void readSettingsFromTxtFile() throws Exception {

        BufferedReader buf = new BufferedReader(new FileReader("settings.txt"));
        String[] tempArr = new String[7];
        int cnt = 0;
        String line;
        while ((line = buf.readLine()) != null) {
            if (!line.isEmpty() && !line.startsWith("#")) {
                tempArr[cnt] = line;
                cnt++;
            }
        }
        filePath = tempArr[0];
        fileName = tempArr[1];
        remoteCatalog = tempArr[2];
        String server = tempArr[3];
        int port = Integer.parseInt(tempArr[4]);
        String user = tempArr[5];
        String password = tempArr[6];
        authData = new FTPAuthData(server, port, user, password);

    }

}
