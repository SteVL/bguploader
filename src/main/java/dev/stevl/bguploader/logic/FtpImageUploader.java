package dev.stevl.bguploader.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author Vladislav Stebenev
 */
public class FtpImageUploader implements ImageUploader {
    @Override
    public void uploadImage(File fileToUpload, String remotePath, FTPAuthData authData) throws Exception {                
        FTPClient ftpClient = new FTPClient();
        ftpClient.connect(authData.getServer(), authData.getPort());
        ftpClient.login(authData.getUser(), authData.getPassword());
        ftpClient.enterLocalPassiveMode();

        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        
        File localFile = fileToUpload;
        System.out.println("I'm taking file: "+fileToUpload.getAbsolutePath());
        
        String remoteFileName = remotePath+"/"+fileToUpload.getName();
        System.out.println("I'm taking remote catalog and filename: "+remoteFileName);
        
        InputStream inputStream = new FileInputStream(localFile);
        
      
        System.out.println("Uploading file...");
        boolean done = ftpClient.storeFile(remoteFileName, inputStream);
        inputStream.close();
        if (done) {
            System.out.println("File is uploaded successfully.");
        }

    }
}
