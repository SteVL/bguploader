package dev.stevl.bguploader.logic;

/**
 * Описывает какие настроки нужны для исполнения скрипта
 * @author Vladislav Stebenev
 */
public interface SettingsHandler {

    String getFilePath();

    String getFileName();
    
    String getRemoteCatalog();

    FTPAuthData getAuthData();
}
