package dev.stevl.bguploader.logic;

import java.io.File;

/**
 * Описывает требования к загрузчику изображения на сервер
 *
 * @author Vladislav Stebenev
 */
public interface ImageUploader {        
    void uploadImage(File fileToUpload, String remotePath, FTPAuthData authData) throws Exception;
}
