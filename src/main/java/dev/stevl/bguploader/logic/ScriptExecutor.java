package dev.stevl.bguploader.logic;

import java.io.File;

/**
 *
 * @author Vladislav Stebenev
 */
public class ScriptExecutor {

    public void execute() throws Exception {
        System.out.println("Hi, there! I'll UPLOAD the background for Kosmos instead of you");
        System.out.println("Starting to execute script...");
        System.out.println("Getting settings");
        SettingsHandler settings = new FileSettingsHandler();
//        System.out.println(settings.getFilePath());
//        System.out.println(settings.getFileName());
//        System.out.println(settings.getRemoteCatalog());
//        System.out.println(settings.getAuthData().getServer());
//        System.out.println(settings.getAuthData().getPort());
//        System.out.println(settings.getAuthData().getUser());
//        System.out.println(settings.getAuthData().getPassword());
        
        ImageUploader uploader = new FtpImageUploader();
        uploader.uploadImage(new File(settings.getFilePath()+"/"+settings.getFileName()), settings.getRemoteCatalog(), settings.getAuthData());
        System.out.println("Script is done!");
    }
}
